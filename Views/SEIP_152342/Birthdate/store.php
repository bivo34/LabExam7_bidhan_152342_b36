<?php
require_once("../../../vendor/autoload.php");
use App\Birthdate\Birthdate;

?>
<html>

<head>
    <link rel="stylesheet" href="../../../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../bootstrap/js/bootstrap.min.js">
    <script src="../../../bootstrap/js/jquery.min.js"></script>
    <script src="../../../bootstrap/js/bootstrap.min.js"></script>
    <link href="../../../style/style.css" rel="stylesheet" type="text/css">

</head>
<body>
<div class="alert-success">
    <?php
    $obj=new Birthdate();
    $obj->setData($_POST);
    $obj->store();
    ?>
</div>
    