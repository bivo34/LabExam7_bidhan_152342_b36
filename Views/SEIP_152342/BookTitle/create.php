<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

?>

<!DOCTYPE html>
<html lang="en">

<head><title>Atomic Project </title>
    <link href="../../../style/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../../../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../bootstrap/js/bootstrap.min.js">
    <script src="../../../bootstrap/js/jquery.min.js"></script>
    <script src="../../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../JS/create.js"></script>
    <script type="text/javascript" src="../../../bootstrap/js/bootstrapValidator.js"></script>
    <link rel="stylesheet" href="../../../bootstrap/css/bootstrapValidator.css"/>
    <link rel="stylesheet" href="../../../font-awesome-4.7.0/css/font-awesome.min.css"/>
   
</head>
<body>
<div class="container" id="contain">




    <div class="page-header" id="message">
<?php echo Message::message(); ?>
    </div>
    <div class="row">


        <div class="col-md-9 col-lg-9 col-sm-9" >
            <div class="panel panel-default">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="btn btn-warning"  data-toggle="modal" data-target="#myModalNorm">
                            <span class="glyphicon glyphicon-plus">Add Book Title</span></button>


                    </div>
                </div>

            </div>
        </div>
        <div class="modal fade" id="myModalNorm" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h3>Book
                            <i class="fa fa-book fa-lg " aria-hidden="true"></i>
                        </h3>

                        <button type="button" class="close"
                                data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                    </div>

                    <!-- Modal Body -->
                    <div class="modal-body">

                        <form action="store.php" id="bookform" method="post" role="form">
                            <div class="form-group">
                                <label for="book_title">Book Title</label>
                                <input type="text" class="form-control" name="book_title"
                                       id="book_title" placeholder="Enter book_title.."/>
                            </div>
                            <div class="form-group">
                                <label for="author_name">Author Name</label>
                                <input type="text" class="form-control" name="author_name"
                                       id="author_name" placeholder="Enter author_nam..e"/>
                            </div>
                            <button type="submit" class="btn btn-info" onclick="myFunction()">Submit</button>


                        </form>
                    </div>
                    </div>
                </div>
            </div>



                </div>



</body>

</html>