<?php
namespace App\Birthdate;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
if(!isset($_SESSION))session_start();
//use PDO;

class Birthdate extends DB
{
    public $id;
    public $user_name;
    public $birth_date;

    public function __construct()
    {
        parent::__construct();

    }

    public function setData($postVariabledata = NULL)
    {
        if (array_key_exists("id", $postVariabledata)) {
            $this->id = $postVariabledata['id'];

        }
        if (array_key_exists("user_name", $postVariabledata)) {
            $this->user_name = $postVariabledata['user_name'];
        }
        if (array_key_exists("birth_date", $postVariabledata)) {
            $this->birth_date = $postVariabledata['birth_date'];
        }

    }
    public function store(){
        $arrData=array($this->user_name,$this->birth_date);
        $sql="insert into birthday(user_name,birth_date)VALUES
            (?,?)";
        $STH= $this->DBH->prepare($sql);
        $result= $STH->execute($arrData);
        if($result==NULL)
            Message::message("Failed! DATA HAS not BEEN INSERTED SUCCESSFULLY");
        else
            Message::message("Success! DATA HAS BEEN INSERTED SUCCESSFULLY");
        Utility::redirect('create.php');
    }
}




