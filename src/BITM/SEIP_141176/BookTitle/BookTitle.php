<?php
namespace App\BookTitle;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
if(!isset($_SESSION))session_start();
use PDO;

class BookTitle extends DB
{
    public $id;
    public $author_name;
    public $book_title;

    public function __construct()
    {
        parent::__construct();

    }

    public function setData($postVariabledata = NULL)
    {
        if (array_key_exists("id", $postVariabledata)) {
            $this->id = $postVariabledata['id'];

        }
        if (array_key_exists("book_title", $postVariabledata)) {
            $this->book_title = $postVariabledata['book_title'];
        }
        if (array_key_exists("author_name", $postVariabledata)) {
            $this->author_name = $postVariabledata['author_name'];
        }

    }
    public function store(){
        $arrData=array($this->book_title,$this->author_name);
        $sql="insert into book_title(book_title,author_name)VALUES
            (?,?)";
       $STH= $this->DBH->prepare($sql);
       $result= $STH->execute($arrData);
        if($result==NULL)
            Message::message("Failed! DATA HAS not BEEN INSERTED SUCCESSFULLY");
        else
            Message::message("Success! DATA HAS BEEN INSERTED SUCCESSFULLY");
        Utility::redirect('create.php');
    }



    public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from book_title ORDER BY id DESC ');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();






    public function view($fetchMode='ASSOC'){

        $sql = 'SELECT * from book_title where id='.$this->id;

        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();


    public function update()
    {
        $arrData  = array($this->book_title,$this->author_name);

        $sql = 'UPDATE book_title  SET book_title  = ? , author_name = ? where id ='.$this->id;

        $STH = $this->DBH->prepare($sql);


        $STH->execute($arrData);


        Utility::redirect('index.php');
    }

    public function delete(){

        $sql = "Delete from book_title where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of delete()

    public function trash(){

        $sql = "Update book_title SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()

}




