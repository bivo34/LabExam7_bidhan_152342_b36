<?php
namespace App\Email;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
if(!isset($_SESSION))session_start();
//use PDO;

class Email extends DB
{
    public $id;
    public $user_name;
    public $email;

    public function __construct()
    {
        parent::__construct();

    }

    public function setData($postVariabledata = NULL)
    {
        if (array_key_exists("id", $postVariabledata)) {
            $this->id = $postVariabledata['id'];

        }
        if (array_key_exists("username", $postVariabledata)) {
            $this->user_name = $postVariabledata['username'];
        }
        if (array_key_exists("user_email", $postVariabledata)) {
            $this->email = $postVariabledata['user_email'];
        }

    }
    public function store(){
        $arrData=array($this->user_name,$this->email);
        $sql="insert into email(user_name,email)VALUES
            (?,?)";
        $STH= $this->DBH->prepare($sql);
        $result= $STH->execute($arrData);
        if($result==NULL)
            Message::message("Failed! DATA HAS not BEEN INSERTED SUCCESSFULLY");
        else
            Message::message("Success! DATA HAS BEEN INSERTED SUCCESSFULLY");
        Utility::redirect('create.php');
    }
}




