<?php
namespace App\Gender;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
if(!isset($_SESSION))session_start();
//use PDO;

class Gender extends DB
{
    public $id;
    public $user_name;
    public $gender;

    public function __construct()
    {
        parent::__construct();

    }

    public function setData($postVariabledata = NULL)
    {
        if(gender_male.checked==true)
        {
            $this->gender="Male";
        }
        if (gender_female==true){
            $this->gender="Female";
        }
        if (array_key_exists("id", $postVariabledata)) {
            $this->id = $postVariabledata['id'];

        }
        if (array_key_exists("user_name", $postVariabledata)) {
            $this->user_name = $postVariabledata['user_name'];
        }
        if (array_key_exists("gender", $postVariabledata)) {
            $this->gender = $postVariabledata['gender'];
        }

    }
    public function store(){
        $arrData=array($this->user_name,$this->gender);
        $sql="insert into gender(user_name,gender)VALUES(?,?)";
        $STH= $this->DBH->prepare($sql);
        $result= $STH->execute($arrData);
        if($result)
            Message::message("Success! DATA HAS BEEN INSERTED SUCCESSFULLY");

        else
            Message::message("Failed! DATA HAS not BEEN INSERTED SUCCESSFULLY");
        Utility::redirect('create.php');
    }
}




