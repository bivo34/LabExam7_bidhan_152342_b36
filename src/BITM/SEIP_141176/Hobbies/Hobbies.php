<?php
namespace App\Hobbies;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
if(!isset($_SESSION))session_start();
use PDO;

class Hobbies extends DB
{
    public $id;
    public $user_name;
    public $hobby;

    public function __construct()
    {
        parent::__construct();

    }

    public function setData($postVariabledata = NULL)
    {
        if (array_key_exists("id", $postVariabledata)) {
            $this->id = $postVariabledata['id'];

        }
        if (array_key_exists("username", $postVariabledata)) {
            $this->user_name = $postVariabledata['username'];
        }
        if (array_key_exists("hobbies", $postVariabledata)) {
            $this->hobby = implode(',', $postVariabledata['hobbies']);
        }

    }
    public function store(){
        $arrData=array($this->user_name,$this->hobby);
        $sql="insert into hobbies(user_name,hobby)VALUES
            (?,?)";
        $STH= $this->DBH->prepare($sql);
        $result= $STH->execute($arrData);
        if($result==NULL)
            Message::message("Failed! DATA HAS not BEEN INSERTED SUCCESSFULLY");
        else
            Message::message("Success! DATA HAS BEEN INSERTED SUCCESSFULLY");
        Utility::redirect('create.php');
    }

    public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from hobbies ORDER BY id DESC ');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();

    public function view($fetchMode='ASSOC'){

        $sql = 'SELECT * from hobbies where id='.$this->id;

        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();


    public function update()
    {
        $arrData  = array($this->hobby,$this->user_name);

        $sql = 'UPDATE hobbies  SET hobby  = ?   , user_name = ? where id ='.$this->id;

        $STH = $this->DBH->prepare($sql);


        $STH->execute($arrData);


        Utility::redirect('index.php');
    }



}




