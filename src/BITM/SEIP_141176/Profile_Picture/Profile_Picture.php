<?php
namespace App\Profile_Picture;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
if(!isset($_SESSION))session_start();
//use PDO;

class Profile_Picture extends DB
{
    public $id;
    public $user_name;
    public $image;

    public function __construct()
    {
        parent::__construct();

    }

    public function setData($postVariabledata = NULL)
    {
        if (array_key_exists("id", $postVariabledata)) {
            $this->id = $postVariabledata['id'];

        }
        if (array_key_exists("user_name", $postVariabledata)) {
            $this->user_name = $postVariabledata['user_name'];
        }
        if (array_key_exists("image", $postVariabledata)) {

            $this->image =  $_POST['image'];
        }

    }
    public function store(){
        $arrData=array($this->user_name,$this->image);
        $sql="insert into profile_picture(user_name,image  )VALUES(?,?)";
        $STH= $this->DBH->prepare($sql);
        $result= $STH->execute($arrData);
        if($result)
            Message::message("Success! DATA HAS BEEN INSERTED SUCCESSFULLY");
        else
            Message::message("Failed! DATA HAS not BEEN INSERTED SUCCESSFULLY");
        Utility::redirect('create.php');
    }
}




